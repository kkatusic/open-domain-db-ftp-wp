<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

include 'vendor/autoload.php';

include 'config.php';

use Hackzilla\PasswordGenerator\Generator\ComputerPasswordGenerator;
use Touki\FTP\Connection\Connection;
use Touki\FTP\FTPWrapper;
use Touki\FTP\FTPFactory;
use Touki\FTP\FTP;
use Touki\FTP\Model\File;

$status = false;

if( isset($_POST) ){

    //set up variable
    $WHMUsername    =   filter_var($_POST['username'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $WHMPassword    =   filter_var($_POST['password'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $domain         =   filter_var($_POST['domain'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

    $generator = new ComputerPasswordGenerator();

    $generator
        ->setOptionValue(ComputerPasswordGenerator::OPTION_UPPER_CASE, true)
        ->setOptionValue(ComputerPasswordGenerator::OPTION_LOWER_CASE, true)
        ->setOptionValue(ComputerPasswordGenerator::OPTION_NUMBERS, true)
        ->setOptionValue(ComputerPasswordGenerator::OPTION_SYMBOLS, false)
    ;

    $generator
        ->setUppercase()
        ->setLowercase()
        ->setNumbers()
        ->setSymbols(true)
        ->setLength(16);

    $generatedPassword = $generator->generatePassword();

    $generatedUsername = substr(str_replace('.', '', strtolower($domain)), 0, 8);

    /* CREATE NEW ACCOUNT */
    $argumentsCreate = array(
        'api.version'   =>  '1',
        'user'          =>  $generatedUsername,
        'domain'        =>  $domain,
        'password'      =>  $generatedPassword,
        'contactemail'  =>  $configData->domainEmail
    );

    $whm = new \Gufy\CpanelPhp\Cpanel([
        'host'        =>  $configData->WHMHost, // ip or domain complete with its protocol and port
        'username'    =>  $WHMUsername, // username of your server, it usually root.
        'auth_type'   =>  'password', // set 'hash' or 'password'
        'password'    =>  $WHMPassword, // long hash or your user's password
    ]);

    //create IT
    $account = $whm->createacct($argumentsCreate);

    //account created => create Database and database User
    if ( $account ) {

        $databaseName = $generatedUsername . $configData->DBNameSuffix;

        $dataDB = $whm->execute_action('2', 'MysqlFE', 'createdb', $generatedUsername, ['db' => $databaseName]);

        //database created => create database user
        if ( $dataDB ) {

            $databaseUser           =   $generatedUsername . $configData->DBUsernameSuffix;
            $generatedPasswordDB    =   $generator->generatePassword();

            $dataDBUserArguments = [
                'dbuser'    =>  $databaseUser,
                'password'  =>  $generatedPasswordDB
            ];

            $dataDBUser = $whm->execute_action('2', 'MysqlFE', 'createdbuser', $generatedUsername, $dataDBUserArguments);

            //database user created => add database user privileges
            if( $dataDBUser ) {

                $DBUserPrivilegesArguments = [
                    'privileges'    =>  'ALL PRIVILEGES',
                    'db'            =>  $databaseName,
                    'dbuser'        =>  $databaseUser
                ];

                $dataDBUserPrivileges = $whm->execute_action('2', 'MysqlFE', 'setdbuserprivileges', $generatedUsername, $DBUserPrivilegesArguments);

                //database user has privileges => create FTP account
                if ( $dataDBUserPrivileges ) {

                    $ftpUserName = $configData->FTPUsernamePrefix;
                    $ftpPassword = $generator->generatePassword();

                    $ftpUserArgs = [
                        'user'      =>  $ftpUserName,
                        'pass'      =>  $ftpPassword,
                        'quota'     =>  '0', //unlimited
                        'homedir'   =>  '/', //root directory
                    ];

                    $dataFTPUser = $whm->execute_action('2', 'Ftp', 'addftp', $generatedUsername, $ftpUserArgs);

                    //FTP account created => upload getWP.php and execute that script
                    if ($dataFTPUser) {

                        $connection = new Connection($configData->FTPDomain, $ftpUserName . '@' . $domain, $ftpPassword, $port = 21, $timeout = 900, $passive = true);
                        $connection->open();

                        $wrapper = new FTPWrapper($connection);
                        $factory = new FTPFactory;

                        $ftp = $factory->build($connection);

                        $wrapper->chdir("/public_html");

                        //Upload getWP.php gile
                        $ftp->upload(new File('getWP.php'), __DIR__ . '/getWP.php');

                        $connection->close();

                        //sleep for 60 seconds because I said so
                        sleep(60);

                        // create a new cURL resource
                        $ch = curl_init();

                        //set URL and other appropriate options
                        curl_setopt($ch, CURLOPT_URL, "http://162.255.162.172/~" . $generatedUsername . "/getWP.php");
                        curl_setopt($ch, CURLOPT_HEADER, 0);

                        //grab URL and pass it to the browser
                        curl_exec($ch);

                        //close cURL resource, and free up system resources
                        curl_close($ch);


                        $status = true;//END is here
                    }

                }

            }


        }

    }

}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Created user</title>
</head>
<body>

    <?php if( $status ): ?>

        <table border="1" cellpadding="10">
            <tr>
                <td>Domain: </td>
                <td><?php echo $domain ?></td>
            </tr>
            <tr>
                <td colspan="2">cPanel data</td>
            </tr>
            <tr>
                <td>Username: </td>
                <td><?php echo $generatedUsername; ?></td>
            </tr>
            <tr>
                <td>Password: </td>
                <td><?php echo $generatedPassword; ?></td>
            </tr>
            <tr>
                <td colspan="2">Database data</td>
            </tr>
            <tr>
                <td>Database name: </td>
                <td><?php echo $databaseName; ?></td>
            </tr>
            <tr>
                <td>Database username: </td>
                <td><?php echo $databaseUser; ?></td>
            </tr>
            <tr>
                <td>Database username password: </td>
                <td><?php echo $generatedPasswordDB; ?></td>
            </tr>
            <tr>
                <td colspan="2">FTP data</td>
            </tr>
            <tr>
                <td>FTP username: </td>
                <td><?php echo $ftpUserName; ?></td>
            </tr>
            <tr>
                <td>FTP password: </td>
                <td><?php echo $ftpPassword; ?></td>
            </tr>
        </table>

    <?php else: ?>
        <h2 style="color: red;">Error!!!</h2>
    <?php endif; ?>

</body>
</html>
