<?php

/**
 * Copy a file, or recursively copy a folder and its contents
 * @author      Aidan Lister <aidan@php.net>
 * @version     1.0.1
 * @link        http://aidanlister.com/2004/04/recursively-copying-directories-in-php/
 * @param       string   $source    Source path
 * @param       string   $dest      Destination path
 * @param       int      $permissions New folder creation permissions
 * @return      bool     Returns true on success, false on failure
 */
function xcopy($source, $dest, $permissions = 0755)
{
    // Check for symlinks
    if (is_link($source)) {
        return symlink(readlink($source), $dest);
    }

    // Simple copy for a file
    if (is_file($source)) {
        return copy($source, $dest);
    }

    // Make destination directory
    if (!is_dir($dest)) {
        mkdir($dest, $permissions);
    }

    // Loop through the folder
    $dir = dir($source);
    while (false !== $entry = $dir->read()) {
        // Skip pointers
        if ($entry == '.' || $entry == '..') {
            continue;
        }

        // Deep copy directories
        xcopy("$source/$entry", "$dest/$entry", $permissions);
    }

    // Clean up
    $dir->close();
    return true;
}

/**
 * Removes child directories and its contents
 *
 * @param string $path Path to the directory.
 */
function _delete_dir_files($dir) {
    if (is_dir($dir)) {
        $scn = scandir($dir);
        foreach ($scn as $files) {
            if ($files !== '.') {
                if ($files !== '..') {
                    if (!is_dir($dir . '/' . $files)) {
                        unlink($dir . '/' . $files);
                    } else {
                        _delete_dir_files($dir . '/' . $files);
                        rmdir($dir . '/' . $files);
                    }
                }
            }
        }
    }
}

//Get latest WordPress ZIP file
$f = file_put_contents("wpzip.zip", fopen("https://wordpress.org/latest.zip", 'r'), LOCK_EX);

if( FALSE === $f ) {
    die("Couldn't write to ZIP file.");
}

//open WP zip archive
$zip = new ZipArchive;
$res = $zip->open('wpzip.zip');

if ($res === TRUE) {

    //unzip it to destination
    $zip->extractTo('./');
    $zip->close();

    //delete wordpress directory
    $return = xcopy('wordpress', './');

    if( $return ){

        if( is_dir('wordpress') ){

            _delete_dir_files('wordpress');
            rmdir('wordpress');

        }

    }

    //delete zip file
    unlink("wpzip.zip");

    //delete this script
    unlink("getWP.php");

}
else {
    die("Couldn't open ZIP file.");
}