<?php

/* Config object */
$configData = new stdClass();

/* Domain email */
$configData->domainEmail = 'kkatusic@gmail.com';

/* WHM host and port */
$configData->WHMHost = 'https://host.somevps.com:2087/';

/* DB name suffix */
$configData->DBNameSuffix = '_wpdb';

/* DB username suffix */
$configData->DBUsernameSuffix = '_wpAdmin';

/* FTP username prefix */
$configData->FTPUsernamePrefix = 'kkatusic';

/* FTP Domain or IP */
$configData->FTPDomain = 'somevps.com';
